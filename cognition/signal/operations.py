import numpy as np


def shift(x, n, pad=0):
    # type: (np.ndarray, int, float) -> np.ndarray
    if n == 0:
        return x
    e = np.empty_like(x)
    if n >= 0:
        e[:n] = pad
        e[n:] = x[:-n]
    else:
        e[n:] = pad
        e[:n] = x[-n:]
    return e


def normalize(x):
    # type: (np.ndarray) -> np.ndarray
    ret = x / sum(np.asarray(x, dtype=float))
    return ret


def convolve1d(x, k, offset=0, mode='constant', norm=True):
    # type: (np.ndarray, np.ndarray, int, str, bool) -> np.ndarray
    assert (x.ndim == k.ndim)
    assert (x.ndim == 1)

    if mode == 'constant':
        x = shift(x, offset, 0.0)
    elif mode == 'wrap':
        x = np.roll(x, offset)
    else:
        raise ValueError

    k = np.flip(k, axis=0)
    k_width = len(k)
    padding_amount = int(k_width / 2)
    pad_x = np.pad(x, padding_amount, mode)
    ret = np.zeros(x.shape)

    for i in range(pad_x.shape[0] - k_width + 1):
        tmp = pad_x[i:i + k_width]
        ret[i] = np.sum(tmp * k)
    if norm:
        ret = normalize(ret)
    return ret
