import numpy as np

from .operations import normalize


def gaussian(shape, sigma, norm=True):
    # type: ((int, int) or int, float, bool) -> np.ndarray
    if np.isscalar(shape):
        ndim = 1
        n = shape
    else:
        ndim = len(shape)
        assert ndim == 2, 'Can only generate 1D or 2D kernels'
        m, n = shape[0], shape[1]
    if ndim == 1:
        x = np.arange(-int(n / 2), int(n / 2) + 1)
        k = np.exp(-(x ** 2) / (2 * sigma ** 2))
    elif ndim == 2:
        xr = np.arange(-int(n / 2), int(n / 2) + 1)
        yr = np.arange(-int(m / 2), int(m / 2) + 1)
        x, y = np.meshgrid(xr, yr)
        k = np.exp(-(x ** 2 + y ** 2) / (2 * sigma ** 2))
    if norm:
        return normalize(k)
    else:
        return k
