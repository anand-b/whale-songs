"""
Operations on the frequency domain of a signal
"""
from .fourier import fourier_spectrum
from .spectrogram import filter_spectrogram 
