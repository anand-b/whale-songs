import numpy as np

from ..kernels import gaussian
from ..operations import convolve1d


def filter_spectrogram(fmin, fmax, f, Sxx):
    # type: (float, float, np.ndarray, np.ndarray) -> (np.ndarray, np.ndarray)
    """
    Filter a frequency range from a Spectrogram
    :param fmin: Lower bound of frequency range (Hz)
    :param fmax: Upper bound of frequency range (Hz)
    :param f: Frequency segments returned from scipy spectrogram
    :param Sxx: scipy Spectrogram
    :return: Filtered frequency segment and spectrogram
    """
    fslice = np.where((f >= fmin) & (f <= fmax))
    return f[fslice], Sxx[fslice, :][0]


def spectrogram_distribution(Sxx):
    # type: (np.ndarray) -> np.ndarray
    hist = np.sum(Sxx, axis=1)  # Sum up the frequency distributions
    return hist / np.sum(hist)


def threshold_value(Sxx, threshold):
    return np.where(Sxx > threshold, 1, 0)


def zero_franges(f, Sxx, ranges):
    Sret = np.array(Sxx)
    fret = np.array(f)
    for r in ranges:
        fmin, fmax = r[0], r[1]
        fslice = np.where((f >= fmin) & (f <= fmax))
        Sret[fslice, :] = 0
    return fret, Sret


def get_projections(Sxx):
    proj = np.array([np.max(col) for col in Sxx.T], dtype=bool)
    return np.asarray(proj, dtype=int)


def smooth_projection(proj, min_len=4, k=None):
    # type: (np.ndarray or list, int, np.ndarray or list) -> np.ndarray
    # ret = np.asarray(np.array(proj, dtype=bool), dtype=int)

    if k is None:
        k = gaussian(3, sigma=5)
    ret = np.asarray(np.asarray(convolve1d(proj, k), dtype=bool), dtype=int)

    begin_idx = end_idx = 0
    on = False
    for i, p in enumerate(ret):
        if not on and p == 1:
            begin_idx = i
            on = True
        elif on and p == 0:
            end_idx = i
            on = False
            if end_idx - begin_idx < min_len:
                ret[begin_idx:end_idx] = 0
    return ret


def get_durations(t, proj):
    # type: (np.ndarray or list, np.ndarray or list) -> list[tuple[float, float]]
    durations = []
    on = False
    begin = end = 0
    for i, d in enumerate(proj):
        if not on and d != 0:
            begin = t[i]
            on = True
        elif on and d == 0:
            durations.append((begin, t[i] - begin))
            on = False
    return durations
