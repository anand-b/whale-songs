import numpy as np
from scipy.fftpack import fft


def fourier_spectrum(y, Fs):
    # type: (np.ndarray, float) -> (np.ndarray, np.ndarray)
    n = len(y)  # length of the signal
    k = np.arange(n)
    T = n / Fs
    frq = k / T  # two sides frequency range
    Y = fft(y) / n  # fft computing and normalization
    return frq, Y