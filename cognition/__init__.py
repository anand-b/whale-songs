"""
Package used for Analysis of Whale Songs (PSY446: Animal Cognition)
"""

from .data import load_from_dir, load_file

__all__ = ['load_file', 'load_from_dir']

