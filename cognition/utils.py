import numpy as np


def normalize(x):
    # type: (np.ndarray) -> np.ndarray
    ret = x / sum(np.asarray(x, dtype=float))
    return ret
