import os

import scipy.io.wavfile


def load_file(wav):
    fname = os.path.splitext(os.path.basename(wav))[0]
    ext = os.path.splitext(os.path.basename(wav))[1]
    if ext != '.wav':
        # print('File ext is ', ext) 
        return None
    fs, data = scipy.io.wavfile.read(wav)
    ret = {
            'name': fname,
            'fs': fs,
            'data': data
            }
    return ret

def load_from_dir(dir):
    ret = []
    # print(os.listdir(dir))
    for filename in os.listdir(dir):
        f = os.path.join(dir, filename)
        d = load_file(f)
        if d is not None:
            print('Loading file: {}'.format(f))
            ret.append(d)
    return ret
