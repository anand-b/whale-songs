import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from scipy.fftpack import fft


def display_spectrogram(t, f, Sxx, show=False):
    # type: (np.ndarray, np.ndarray, np.ndarray, bool) -> None
    """
    Display spectrogram

    :param t: Time segment from spectrogram
    :param f: Frequency segment from spectrogram
    :param Sxx: Spectrogram
    """
    plt.pcolormesh(t, f, Sxx)
    plt.ylabel('Frequency [Hz]')
    plt.xlabel('Time [sec]')
    if show:
        plt.show()


def display_spectrum(frq, Y, show=False):
    # type: (np.ndarray, np.ndarray, bool) -> None
    """
    Display the Fourier spectrum of a signal
    :param frq: Frequency bins of the FFT
    :param Y: FFT of the signal
    """
    plt.plot(frq, abs(Y), 'r')  # plotting the spectrum
    plt.xlabel('Freq (Hz)')
    plt.ylabel('|Y(freq)|')
    if show:
        plt.show()



