#!/bin/bash

for pair in $(ls -d $0*/); do
  pair_dir=$(realpath $pair)
  for clip_dir in $(ls -d $pair_dir/*/); do
    # echo "Renaming stuff in $clip_dir"
    clip_name=$(basename $clip_dir)
    echo "Cleaning $clip_name"
    rename "s/sm$clip_name//" $clip_dir/*
    rename "s/100_200/01/" $clip_dir/*
    rename "s/200_400/02/" $clip_dir/*
    rename "s/400_1010/03/" $clip_dir/*
  done
done

