#!/usr/bin/env python3
import argparse
import os
import sys
import json

import numpy as np
import scipy
import scipy.signal as sig

import matplotlib
import matplotlib.pyplot as plt


import cognition as cog
import cognition.disp as cdisp
import cognition.signal.freq.spectrogram as cspec

ranges = [
        [100, 200],
        [200, 400],
        [400, 1010]
        ]


parser = argparse.ArgumentParser(description='Analyse paired wav files in a directory')
parser.add_argument('-i', '--input-dir', help='Directory containing the pairs of dirs')
parser.add_argument('-o', '--output-dir', default='')
args = parser.parse_args()

def main():
    data = []
    data_dir = args.input_dir
    savedir = args.output_dir if args.output_dir != '' else data_dir
    for i, d in enumerate(os.listdir(data_dir)):
        d_ = os.path.join(data_dir, d)
        data = cog.load_from_dir(d_)
        print('Loaded {} file(s) from dir: {}'.format(len(data), d_))
        s1, s2 = data[0], data[1]
        out = os.path.join(savedir, str(i+1))
        print('Saving pair to ', out)
        pair_analysis(s1, s2, out)


def duration_stats(ranges, durations, savefile):
    import functools
    assert(len(ranges) == len(durations))
    print("Saving stats to ", savefile)
    with open(savefile, 'w') as sfile:
        for i, d in enumerate(durations):
            sum_d = functools.reduce((lambda x,y: x + y[1]), d, 0)
            avg_d = sum_d/len(d)
            max_d = functools.reduce((lambda x,y: max(x,y[1])), d, d[0][1])
            n_d = len(d)
            sfile.write('Range [{} - {}] Hz:\n'.format(ranges[i][0], ranges[i][1]))
            sfile.write('\tTotal Duration with actual songs = {} seconds (approx.)\n'.format(sum_d))
            sfile.write('\tAverage Duration of each "note"  = {} seconds (approx.)\n'.format(avg_d))
            sfile.write('\tMax Duration of each "note" = {} seconds (approx.)\n'.format(max_d))
            sfile.write('\tNumber of "notes" in frequency band = {} (approx.)\n'.format(n_d))



def threshold_and_stats(t, f, Sxx, save_prefix):
    # 100-150 Hz, 250-350 Hz and 400-1000 Hz. 
    durations = []
    for i, r in enumerate(ranges):
        fname = save_prefix + '{}_{}'.format(r[0], r[1])
        f_, S_ = cspec.filter_spectrogram(r[0], r[1], f, Sxx)
        plt.figure()
        hist_ = cspec.spectrogram_distribution(S_)
        plt.plot(f_,hist_)
        plt.title('Frequency Distribution of Range')
        plt.xlabel('Frequencies [Hz]')
        plt.ylabel('Normalized Intensities')
        u, sig = np.mean(S_), np.std(S_)
        threshold = u + 2*sig
        Sn = np.where(S_ <= threshold, 0, 1)
        p = cspec.get_projections(Sn)
        print("Saving histogram to ", fname + '_histogram.png')
        plt.savefig(fname + '_histogram.png')

        plt.figure()
        cdisp.display_spectrogram(t, f_, Sn)
        print("Saving thresholded spectrogram to ", fname + '_thresholded.png')
        plt.savefig(fname + '_thresholded.png')

        p_ = cspec.smooth_projection(p, k=np.array([0, 0, 1, 0, 1]))
        d = cspec.get_durations(t, p_)
        durations.append(d)

    with open(save_prefix + '_durations.json', 'w') as saveFile:
        json.dump(durations, saveFile, indent=4)
    duration_stats(ranges, durations, save_prefix + '_stats.txt')


def pair_analysis(s1_data, s2_data, save_dir='data/output'):
    if not os.path.isdir(save_dir):
        os.makedirs(save_dir)
    fs1 = s1_data['fs']
    s1 = s1_data['data'][:,0]
    f1, t1, Sx1 = sig.spectrogram(s1, fs1, nperseg=5000)
    f1, Sx1 = cspec.filter_spectrogram(0, 1010, f1, Sx1)

    fs2 = s2_data['fs']
    s2 = s2_data['data'][:,0]
    f2, t2, Sx2 = sig.spectrogram(s2, fs2, nperseg=5000)
    f2, Sx2 = cspec.filter_spectrogram(0, 1010, f2, Sx2)

    # Plot spectrograms side by side
    fig, ax = plt.subplots()
    ax.pcolormesh(t1, f1, Sx1)
    ax.set_title('Spectrogram of: {}'.format(s1_data['name']))
    ax.set_xlabel('Time [s]')
    ax.set_ylabel('Frequency [Hz]')
    fname = s1_data['name']
    fname = os.path.join(save_dir, fname)
    print('Saving spectrogram to ', fname + '_spectrogram.png')
    fig.savefig(fname + '_spectrogram.png')
    threshold_and_stats(t1,f1,Sx1, fname)

    fig, ax = plt.subplots()
    ax.pcolormesh(t2, f2, Sx2)
    ax.set_title('Spectrogram of: {}'.format(s2_data['name']))
    ax.set_xlabel('Time [s]')
    ax.set_ylabel('Frequency [Hz]')
    fname = s2_data['name']
    fname = os.path.join(save_dir, fname)
    print('Saving spectrogram to ', fname + '_spectrogram.png')
    fig.savefig(fname + '_spectrogram.png')
    threshold_and_stats(t2,f2,Sx2, fname)


if __name__ == '__main__':
    main()
